﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Unit3_Platform
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            this.lbl.Text = Device.OnPlatform<string>("iOS", "Android", "WinPhone");
        }
    }
}
